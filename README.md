# Devuan image

Uses debootstrap to install a vanilla devuan into a container image.


## Use

In the simplest case, you can just
```
docker run --rm -ti registry.gitlab.com/yjftsjthsd-g/docker_devuan
```
to get ceres (unstable), or
```
docker run --rm -ti registry.gitlab.com/yjftsjthsd-g/docker_devuan:chimaera
```
for a specific release.

If you wish to build locally, clone this repo and run `make`.

## TODO

* Should I set CMD to BASH?
* Is there a config option I could inject to the final image to default to no-install-recommends?
    * yes: https://superuser.com/questions/615565/can-i-make-apt-get-always-use-no-install-recommends
    * could even make my own "-minimal" tags
* Is there a good way to install less in the target? (i.e. I do not need dmidecode in a container)
* Update Makefile to take arch and release as environment variables
    * Probably make CI config just run make


## License

This repo is MIT (see LICENSE file), but note that that only covers the contents
of this repo (builds scripts, basically), not the contents of the generated
image!
