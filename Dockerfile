FROM alpine as build
RUN apk add -U dpkg ca-certificates perl wget gpgv git make
RUN mkdir /target

RUN git clone --depth 1 https://git.devuan.org/devuan/devuan-keyring.git /root/devuan-keyring
RUN cp -r /root/devuan-keyring/keyrings /usr/share/

RUN git clone --depth 1 https://git.devuan.org/devuan/debootstrap.git /root/debootstrap
RUN cd /root/debootstrap ; \
        make DESTDIR=/ install

ARG arch=amd64
ARG release=ceres

RUN debootstrap --arch $arch $release /target http://deb.devuan.org/merged/ $release

FROM scratch as output
LABEL maintainer "Brian Cole <docker@brianecole.com>"
COPY --from=build /target /
CMD /bin/sh
